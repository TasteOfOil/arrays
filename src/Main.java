public class Main {
    public static void main(String[] args) {
        int[] arr = new int[8];
        int a = 1;
        int b=10;
        for(int i = 0;i<arr.length;i++){
            arr[i] =  (int)(Math.random() * (b-a)  + a);
        }
        for(int i:arr){
            System.out.printf(i+" ");
        }
        boolean flag = true;
        for(int i = 0;i<arr.length-1;i++){
            if(arr[i]>arr[i+1]){
                flag = false;
                break;
            }
        }
        System.out.println();
        if(flag){
            System.out.println("The sequence is strictly increasing");
        }
        else {
            System.out.println("The sequence is not strictly increasing");
        }

        for(int i = 0;i<arr.length;i++){
            if(i%2!=0){
                arr[i] = 0;
            }
        }
        for(int i:arr){
            System.out.printf(i+" ");
        }
    }
}